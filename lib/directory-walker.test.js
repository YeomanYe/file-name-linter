const fs = require('fs')
const path = require('path')
const DirectoryWalker = require('..').DirectoryWalker

const fixturesDir = path.join(__dirname, '..', 'fixtures')
const noop = Function.prototype

describe('DirectoryWalker', () => {
  const emptyDirectory = path.join(fixturesDir, 'empty-directory')
  let directoryWalker

  beforeEach((done) => {
    directoryWalker = new DirectoryWalker()
    fs.mkdir(emptyDirectory, done)
  })

  afterEach((done) => {
    fs.rmdir(emptyDirectory, done)
  })

  it('must list existing files', (done) => {
    const files = []
    const ignoredFiles = []

    directoryWalker.errorCallback = (rootDirectory, err) => done(err)
    directoryWalker.fileCallback = (rootDirectory, filePath) => {
      expect(rootDirectory).toBe(fixturesDir)
      files.push(path.relative(rootDirectory, filePath))
    }
    directoryWalker.ignoreCallback = (filePath) => {
      ignoredFiles.push(path.relative(fixturesDir, filePath))
    }
    directoryWalker.finishCallback = (rootDirectory) => {
      expect(rootDirectory).toBe(fixturesDir)
      expect(files.sort()).toStrictEqual([
        'config.js',
        'config.json',
        'empty-config.json',
        path.join('fails', 'linting'),
        'no-errors-config.json',
        path.join('scripts', 'ignored-folder', 'ignoredFile1.js'),
        path.join('scripts', 'ignoredFile2.js'),
        path.join('scripts', 'one.js'),
        path.join('scripts', 'two.js')
      ])
      done()
    }
    expect(ignoredFiles.sort()).toStrictEqual([])

    directoryWalker.walk(fixturesDir, path.join(fixturesDir, 'ignored'))
  })

  it('must fail for non-existent directory', (done) => {
    const directory = 'does-not-exist'

    directoryWalker.errorCallback = (rootDirectory, err) => {
      expect(rootDirectory).toBe(directory)
      const expectedError = new Error()
      expectedError.errno = -2
      expectedError.code = 'ENOENT'
      expectedError.syscall = 'scandir'
      expectedError.path = directory
      expect(err).toEqual(expectedError)
      done()
    }
    directoryWalker.fileCallback = noop
    directoryWalker.finishCallback = noop

    directoryWalker.walk(directory)
  })

  it('must stop traversing when stat fails', (done) => {
    const directory = __dirname
    const error = new Error('moo')
    const failingCallback = () => done(new Error('Callback must not be called!'))

    directoryWalker.errorCallback = (rootDirectory, err) => {
      expect(rootDirectory).toBe(directory)
      expect(err).toBe(error)
      done()
    }
    directoryWalker.fileCallback = failingCallback
    directoryWalker.finishCallback = failingCallback

    directoryWalker.statCallback(directory, 'doesnt-matter', [], failingCallback, error, null)
  })

  it('must ignore anything but files and directories', (done) => {
    const directory = __dirname
    const stats = {
      isDirectory: () => false,
      isFile: () => false
    }
    const expectedFilePath = 'doesnt-matter'
    const failingCallback = () => done(new Error('Callback must not be called!'))

    directoryWalker.errorCallback = failingCallback
    directoryWalker.fileCallback = failingCallback
    directoryWalker.finishCallback = failingCallback

    directoryWalker.statCallback(directory, expectedFilePath, [], (rootDirectory, filePath) => {
      expect(rootDirectory).toBe(directory)
      expect(filePath).toBe(expectedFilePath)
      done()
    }, null, stats)
  })
})
