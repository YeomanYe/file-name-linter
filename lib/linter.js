class Linter {
  fileNamePattern = /[\s\S]/; // default match all
  dirNamePattern = /[\s\S]/; // default match all
  constructor (fileNamePattern, errorCallback) {
    // only match file
    if (fileNamePattern.startsWith('file:')) {
      fileNamePattern = fileNamePattern.slice(5);
      this.fileNamePattern = new RegExp(fileNamePattern);
      // only match dir
    } else if (fileNamePattern.startsWith('dir:')) {
      fileNamePattern = fileNamePattern.slice(4);
      this.dirNamePattern = new RegExp(fileNamePattern);
      // match file & dir
    } else {
      this.fileNamePattern = new RegExp(fileNamePattern);
      this.dirNamePattern = new RegExp(fileNamePattern);
    }
    this.errorCallback = errorCallback || console.error
  }

  check (fileName, isDir) {
    // test dir
    if (isDir && !this.dirNamePattern.test(fileName)) {
      return this.errorCallback(this.dirNamePattern, fileName)
    }
    // test file
    if (!isDir && !this.fileNamePattern.test(fileName)) {
      return this.errorCallback(this.fileNamePattern, fileName)
    }

    return true;
  }
}

module.exports = Linter
