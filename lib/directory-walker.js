const fs = require('fs')
const path = require('path')

const noop = Function.prototype

class DirectoryWalker {
  constructor (callbacks) {
    callbacks = callbacks || {}
    this.errorCallback = callbacks.errorCallback || console.error
    this.fileCallback = callbacks.fileCallback || console.log
    this.ignoreCallback = callbacks.ignoreCallback || console.log
    this.finishCallback = callbacks.finishCallback || noop
  }

  processDirectory (rootDirectory, directory, ignoredDirectories, done) {
    fs.readdir(directory, this.readdirCallback.bind(this, rootDirectory, directory, ignoredDirectories, done))
  }

  readdirCallback (rootDirectory, directory, ignoredDirectories, done, err, files) {
    if (err) {
      return this.errorCallback(rootDirectory, err)
    }

    let pendingFiles = files.length

    if (!pendingFiles) {
      return done(rootDirectory, directory)
    }

    files.forEach((fileName) => {
      const filePath = path.join(directory, fileName)
      fs.stat(filePath, this.statCallback.bind(this, rootDirectory, filePath, ignoredDirectories, () => {
        --pendingFiles
        if (!pendingFiles) {
          done(rootDirectory, directory)
        }
      }))
    })
  }

  statCallback (rootDirectory, filePath, ignoredDirectories, done, err, stats) {
    if (err) {
      return this.errorCallback(rootDirectory, err)
    }

    if (ignoredDirectories && ignoredDirectories.includes(filePath)) {
      this.ignoreCallback(filePath)
    } else {
      if (filePath.includes(rootDirectory) && stats.isFile()) {
        this.fileCallback(rootDirectory, filePath)
      }

      if (stats.isDirectory()) {
        return this.processDirectory(rootDirectory, filePath, ignoredDirectories, done)
      }
    }

    done(rootDirectory, filePath)
  }
  
  
  walk (rootDirectory, ignoredDirectories) {
    this.processDirectory(rootDirectory, rootDirectory, ignoredDirectories, this.finishCallback);
  }

  walkDirsAlone (rootDirectory, ignoredDirectories, directories) {
    try {
      directories.forEach(directory => {
        if (ignoredDirectories && ignoredDirectories.includes(directory)) {
          this.ignoreCallback(directory)
        } else {
          const stats = fs.statSync(directory);
          // no match exclude
          if (!directory.includes(rootDirectory)) return;
          if (stats.isFile()) {
            this.fileCallback(rootDirectory, directory, false);
          }
    
          if (stats.isDirectory()) {
            this.fileCallback(rootDirectory, directory, true);
          }
        }
      });
    } catch (err) {
      this.errorCallback(rootDirectory, err);
    }
    this.finishCallback(rootDirectory);
  }
}

module.exports = DirectoryWalker
