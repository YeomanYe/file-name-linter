const childProcess = require('child_process')
const path = require('path')

const fixturesDir = path.join(__dirname, '..', 'fixtures')

describe('CLI', () => {
  const run = (args, callback) => {
    const cli = childProcess.fork(path.join(__dirname, '..', 'lib', 'cli'), args, { silent: true })

    let stdoutBuffer = ''
    cli.stdout.on('data', (data) => {
      stdoutBuffer += data
    })

    let stderrBuffer = ''
    cli.stderr.on('data', (data) => {
      stderrBuffer += data
    })

    cli.on('close', (code) => {
      callback(code, stdoutBuffer, stderrBuffer)
    })
  }

  it('must show usage without arguments', (done) => {
    run([], (code, stdout, stderr) => {
      expect(code).toBe(1)
      expect(stdout).toBe('')
      expect(stderr).toBe('Usage: file-name-linter config.json\n')
      done()
    })
  })

  it('must show usage with too many arguments', (done) => {
    run(['one', 'two'], (code, stdout, stderr) => {
      expect(code).toBe(1)
      expect(stdout).toBe('')
      // expect(stderr).toBe('Usage: file-name-linter config.json\n')
      done()
    })
  })
  
  it('must show usage with too few arguments', (done) => {
    run([], (code, stdout, stderr) => {
      expect(code).toBe(1)
      expect(stdout).toBe('')
      expect(stderr).toBe('Usage: file-name-linter config.json\n')
      done()
    })
  })

  it('must show error for non-existent config file', (done) => {
    const fileName = 'non-existent.json'
    run([fileName], (code, stdout, stderr) => {
      expect(code).toBe(1)
      expect(stdout).toBe('')
      expect(stderr).toMatch(`Cannot find module '${path.resolve(fileName)}'\n`)
      done()
    })
  })

  it('must do nothing for empty config file', (done) => {
    const fileName = path.join(fixturesDir, 'empty-config.json')
    run([fileName], (code, stdout, stderr) => {
      expect(code).toBe(0)
      expect(stdout).toBe('')
      expect(stderr).toBe('The config seems to be empty!\n')
      done()
    })
  })

  it('must lint files and ignore paths according to JSON config', (done) => {
    const fileName = path.join(fixturesDir, 'config.json')
    run([fileName], (code, stdout, stderr) => {
      expect(stdout.split('\n').sort()).toStrictEqual([
        '',
        ` 🏁  Finished linting ${path.join('fixtures', 'fails')}`,
        ` 🏁  Finished linting ${path.join('fixtures', 'scripts')}`,
        ` 👀  Ignoring ${path.join('fixtures', 'scripts', 'ignored-folder')}`,
        ` 👀  Ignoring ${path.join('fixtures', 'scripts', path.sep)}ignoredFile2.js`
      ])
      expect(stderr.split('\n').sort()).toStrictEqual([
        '',
        ` ✖  ${path.join('fixtures', 'fails', 'linting')} does not match /some-expression/!`,
        "empty-directory ENOENT: no such file or directory, scandir 'empty-directory'",
        "non-existent ENOENT: no such file or directory, scandir 'non-existent'"
      ])
      expect(code).toBe(1)
      done()
    })
  })

  it('must exit with 0 if no linting errors occurred', (done) => {
    const fileName = path.join(fixturesDir, 'no-errors-config.json')
    run([fileName], (code, stdout, stderr) => {
      expect(stderr).toBe('')
      expect(stdout.split('\n').sort()).toStrictEqual([
        '',
        ` 🏁  Finished linting ${path.join('fixtures', 'scripts', path.sep)}`
      ])
      expect(code).toBe(0)
      done()
    })
  })

  it('must lint files according to JavaScript config', (done) => {
    const fileName = path.join(fixturesDir, 'config.js')
    run([fileName], (code, stdout, stderr) => {
      expect(stderr.split('\n').sort()).toStrictEqual([
        '',
        ` ✖  ${path.join('fails', 'linting')} does not match /^[a-z-/]+\\.js|json$/!`,
        ` ✖  ${path.join('scripts', 'ignored-folder', 'ignoredFile1.js')} does not match /^[a-z-/]+\\.js|json$/!`,
        ` ✖  ${path.join('scripts', 'ignoredFile2.js')} does not match /^[a-z-/]+\\.js|json$/!`
      ])
      expect(stdout.split('\n').sort()).toStrictEqual([
        '',
        ` 🏁  Finished linting ${fixturesDir}`,
        ` 👀  Ignoring ${path.join(fixturesDir, 'ignored')}`
      ])
      expect(code).toBe(1)
      done()
    })
  })
})
