# [file-name-linter](https://gitlab.com/winniehell/file-name-linter/)

Tool / node module that validates whether the file names in one or more directories match one or more conventions.

 [![pipeline status](https://gitlab.com/winniehell/file-name-linter/badges/main/pipeline.svg)](https://gitlab.com/winniehell/file-name-linter/-/commits/main) 
[![coverage report](https://gitlab.com/winniehell/file-name-linter/badges/master/coverage.svg)](https://winniehell.gitlab.io/file-name-linter/coverage/)
[![NPM version](https://img.shields.io/npm/v/file-name-linter.svg)](https://www.npmjs.com/package/file-name-linter)

## Installation

```shell
npm install --save-dev file-name-linter
```

or

```shell
yarn add --dev file-name-linter
```

## Goals

- no external dependencies in production environment
- small size (currently less than 0.2 MB)
- simple config (see [example config])
- allow custom file name patterns
- check all file extensions
- 100% test coverage

[example config]: https://gitlab.com/winniehell/file-name-linter/blob/main/fixtures/config.json

## Alternatives

### [eslint-plugin-filenames](https://github.com/selaux/eslint-plugin-filenames) (no longer maintained)

- [3 dependencies][eslint-plugin-dependencies] (0.2 MB)
- only checks JavaScript files
- can't check file extensions

[eslint-plugin-dependencies]: https://github.com/selaux/eslint-plugin-filenames/blob/1.1.0/package.json#L14-L18

### [fnlint](https://github.com/usabilla/fnlint) (last commit in 2018)

- [5 dependencies][fnlint-dependencies] (6 MB)
- no support for custom file name patterns ([planned](https://github.com/usabilla/fnlint/blob/v1.3.0/README.md#todo))
- can't check file extensions

[fnlint-dependencies]: https://github.com/usabilla/fnlint/blob/v1.3.0/package.json#L49-L55

### [fs-lint](https://github.com/jeanlauliac/fs-lint) (last commit in 2013)

- [6 dependencies][fs-lint-dependencies] (2.2 MB)
- can't check file extensions
- no support for custom file name patterns
- unsure about test coverage

[fs-lint-dependencies]: https://github.com/jeanlauliac/fs-lint/blob/v1.0.0/package.json#L38-L45

### [name-lint](https://github.com/joshwnj/name-lint) (last commit in 2014)

- [2 dependencies][name-lint-dependencies] (0.4 MB)
- can't check file extensions
- unsure about test coverage

[name-lint-dependencies]: https://github.com/joshwnj/name-lint/blob/9634bee96f5344887907480b8224d9cd71d95e5e/package.json#L16-L19

### [rubocop](http://rubocop.readthedocs.io/en/latest/cops_style/#stylefilename)

- only checks Ruby files
- can't check file extensions
